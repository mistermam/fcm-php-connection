<?php
if (!defined('ABSPATH')) {
	exit;
}

define("FCM_SEND_ENDPOINT", "https://fcm.googleapis.com/fcm/send");
define("IID_HOST", "https://iid.googleapis.com/iid");
define("IID_INFO_ENDPOINT", "/info/");
define("IID_ADD_TOPIC_ENDPOINT", "/v1:batchAdd");
define("IID_REMOVE_TOPIC_ENDPOINT", "/v1:batchRemove");

class RootsPushNotifications
{
	private $topic_name = 'ionic';
	private $initiated = false;
	private $key;
	private $headers;
	private $topic;

	public function __construct()
	{
		if (!$this->initiated) {
			$this->key = get_option('fcm_key');
			if( !$this->key ) add_action( 'admin_notices', array($this, 'fcm_key_admin_notice') );
			
			$this->headers = [
				'Content-Type'	=>	'application/json',
				'Authorization'	=>	$this->key
			];
			$this->initiated = true;

			add_action('woocommerce_order_status_changed', array($this, 'order_notifications'), 10, 3);
		}
	}

	/**
	 * FCM key admin notice template
	 */
	public function fcm_key_admin_notice() 
	{
		?>
		<div class="error notice">
			<p><?php _e('Es necesario añadir la clave FCM (Firebase Cloud Messaging) para poder enviar las notificaciones.', ROOTS__PLUGIN_DOMAIN); ?> <a href="/wp-admin/admin.php?page=roots_page#tab-fcm"><?php _e('Configurar', ROOTS__PLUGIN_DOMAIN); ?></a></p>
		</div>
		<?php
	}

	/**
	 * Get info about device
	 * @param string iid_token
	 * @return object response body
	 */
	public function get_iid_info($iid_token)
	{
		$url = IID_HOST . IID_INFO_ENDPOINT . $iid_token;
		$args = [
			'method'	=>	'GET',
			'headers'	=>	$this->headers
		];

		$response = wp_remote_request($url, $args);
		$body = wp_remote_retrieve_body($response);

		return $body;
	}

	/**
	 * Add or remove device to/from topic
	 * @param array tokens
	 * @param boolean remove
	 * @return object response body
	 */
	private function manage_topic($tokens, $remove)
	{
		$url = IID_HOST;
		$url .= ($remove) ? IID_REMOVE_TOPIC_ENDPOINT : IID_ADD_TOPIC_ENDPOINT;
		$args = [
			'method'	=>	'POST',
			'headers'	=>	$this->headers,
			'body'		=> json_encode([
				'to'					=>	"/topics/" . $this->topic,
				'registration_tokens'	=>	$tokens
			])
		];

		$response = wp_remote_request($url, $args);
		$body = wp_remote_retrieve_body($response);

		return $body;
	}

	/**
	 * Add device to topic
	 * @param array tokens
	 */
	public function add_iid_to_topic($tokens)
	{
		$this->manage_topic($tokens, false);
	}

	/**
	 * Remove device from topic
	 * @param array tokens
	 */
	public function remove_iid_from_topic($tokens)
	{
		$this->manage_topic($tokens, true);
	}

	/**
	 * Send message to FCM topic
	 * @param string title
	 * @param string body
	 * @param array data
	 * @return object response
	 */
	public function send_notification_to_topic($title, $body, $data = [], $notID)
	{
		$url = FCM_SEND_ENDPOINT;
		$args = [
			'method'	=>	'POST',
			'headers'	=>	$this->headers,
			'body'		=> json_encode([
				'to'					=>	"/topics/" . $this->topic,
				'data'			=>	[
					'title'		=>	$title,
					'body'		=>	$body,
					'notId'		=>	$notID
				]
			])
		];

		if(count($data) > 0) {
			foreach ($data as $key => $value) {
				$args['body']['data'][$key] = $value;
			}
		}
		$args['body'] = json_encode($args['body']);

		$response = wp_remote_request($url, $args);
		$body = wp_remote_retrieve_body($response);

		return $body;
	}

	/**
	 * Send message to user devices
	 * @param int user_id
	 * @param string title
	 * @param string body
	 * @param array data
	 * @return object response
	 */
	public function send_notification_to_user($user_id, $title, $body, $data = [], $notID)
	{
		$url = FCM_SEND_ENDPOINT;
		$meta = get_user_meta($user_id, 'devices');

		if (count($meta) > 0) {
			$meta = $meta[0];
			$tokens = [];
			foreach ($meta as $token) {
				$tokens[] = $token['token'];
			}

			$args = [
				'method'	=>	'POST',
				'headers'	=>	$this->headers,
				'body'		=> [
					'registration_ids'		=>	$tokens,
					'data'			=>	[
						'title'		=>	$title,
						'body'		=>	$body,
						'notId'		=>	$notID
					]
				]
			];

			if(count($data) > 0) {
				foreach ($data as $key => $value) {
					$args['body']['data'][$key] = $value;
				}
			}
			$args['body'] = json_encode($args['body']);

			$response = wp_remote_request($url, $args);
			$body = wp_remote_retrieve_body($response);
		}
	}

	/**
	 * Send notifications when order status change
	 * @param int order_id
	 * @param string from order status
	 * @param string to order status
	 */
	public function order_notifications($order_id, $from, $to)
	{
		$order = wc_get_order($order_id);

		switch ($to) {
			case 'on-hold':
				$this->send_notification_to_user($order->get_user_id(),
					'Tu pedido ya está en camino!',
					'El pedido #' . $order->get_id() . ' ha sido enviado.',
					[
						'action'	=>	'orders'
					],
					2
				);
				break;
			case 'completed':
				$this->send_notification_to_user($order->get_user_id(),
					'Enhorabuena! Ya has recibido tu pedido.',
					'El pedido #' . $order->get_id() . ' ha sido entregado.',
					[],
					5
				);
				$this->send_notification_to_user($order->get_user_id(),
					'Nos encantaría que valorases tu pedido',
					'Valora el pedido #' . $order->get_id() . '.',
					[
						'action'	=>	'rate-order/' . $order->get_id()
					],
					6
				);
				break;
		}
	}
}